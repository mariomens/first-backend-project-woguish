<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?><?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "login", Array(
	"FORGOT_PASSWORD_URL" => "/authorization/",	// Страница забытого пароля
		"PROFILE_URL" => "/authorization/profile.php",	// Страница профиля
		"REGISTER_URL" => "/authorization/registration.php",	// Страница регистрации
		"SHOW_ERRORS" => "Y",	// Показывать ошибки
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>